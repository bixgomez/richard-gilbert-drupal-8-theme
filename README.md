# Richard Gilbert D8 Starter Theme

This is a theme I built from scratch for my own D8 site, https://richardgilbert.co.

## Specifics

Nothing too special, just something I try to update regularly to feature my latest choice gulp/Sass workflow.
